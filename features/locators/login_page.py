from selenium.webdriver.common.by import By


class LoginPageLocator:
    datavaletLogoLocator = By.ID, "dv-color-logo"
    emailTextBoxElementLocator = By.ID, "email"
    passwordTextBoxElementLocator = By.ID, "password"
    # submitButtonElementLocator = By.CLASS_NAME, "log-in-btn"
    submitButtonElementLocator = By.XPATH, "//span[contains(text(),'CONTINUE')]"

    # forgotPasswordLinkLocator = By.PARTIAL_LINK_TEXT, "Forgot passwor"
    emailLabelLocator = By.XPATH, "//label[@class='label'][contains(text(),'Email')]"
    passwordLabelLocator = By.XPATH, "//label[contains(text(),'Password')]"
    dotHeadingLocator = By.XPATH, "//div[contains(text(),'Welcome to DOT')]"
    contactSupportLocator = By.CLASS_NAME, "support-link.bluish"
    needAssistanceLabelLocator = By.XPATH, "//p[contains(text(),'Need assistance?')]"
    forgotPasswordLinkLocator = By.CLASS_NAME, "font-xs.remove-underline"


    errorMessageLocator = By.ID, "error-message"
    errorMessageForgotPasswordLinkLocator = By.XPATH, "//a[@class='error-message-bold']"

    # profileLink = By.CLASS_NAME, "profile.svelte-ye3lec"
    profileLink = By.CLASS_NAME, "profile.svelte-a1btuc"
    signOutLink = By.XPATH, "//p[contains(text(),'Sign Out')]"
