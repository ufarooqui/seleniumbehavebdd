from selenium.webdriver.common.by import By


class ForgotPasswordPageLocator:
    datavaletLogoLocator = By.ID, "dv-color-logo"
    forgotPasswordHeadingLocator = By.XPATH, "//div[contains(text(),'Forgot Password')]"
    backTologinPageLinkLocator = By.XPATH, "//span[@class='back-to-login-text']"
    forgotEmailIDTextBoxLocator = By.ID, "email-forgot"
    forgotPasswordSendButtonLocator = By.XPATH, "//button[@class='log-in-btn-forgot bg-bluish jsSendButton']"
    forgotPasswordSupportTextLocator = By.CLASS_NAME, "support-text"
    forgotPasswordSupportLinkLocator = By.CLASS_NAME, "font-xs.support-link"
    forgotPasswordLinkLocator = By.XPATH, "//a[@class='font-xs remove-underline']"
    # send_button_locator = By.XPATH, "//button[@class='log-in-btn-forgot bg-bluish jsSendButton']"
    send_button_locator = By.XPATH, "//span[contains(text(),'SEND')]"
    info_message_locator = By.CLASS_NAME, "font-s.line-height-20"

    forgotPasswordEmailLabelLocator = By.XPATH, "//label[@class='label-forgot-pw']"

    incorrectEmailLocator = By.ID, "error-incorrect-email"