from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from features.pages.login_page import LoginPage
from features.pages.forgot_password_page import ForgotPasswordPage
from features.lib.webdriverfactory import WebDriverFactory
import logging
import json


def before_all(context):
    print("executing before all")


def before_feature(context, feature):
    path = 'features/config/conf.json'
    with open(path) as json_data:
        data = json.load(json_data)
    wdf = WebDriverFactory(data)
    context.browser = wdf.get_web_driver_instance()
    context.login_page = LoginPage(context)
    context.forgot_password_page = ForgotPasswordPage(context)
    context.login_page.login()


def after_feature(context, feature):
    context.browser.quit()
