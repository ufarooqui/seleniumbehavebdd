Feature: Forgot Password
  Description: This feature will test Forgot Password Functionality

  Scenario: Verify elements on Forgot Password page
    Given User is Logged out
    When User enters username "test5@aw.ca"
    And User clicks on continue Button
    And User clicks on Forgot Password link
    Then Verify heading should be Forgot Password
    And Verify Back to Login link is displayed
    And Verify email label should be displayed
    And Verify forgot email textbox is present
    And Verify send button is present


  Scenario: Verify Contact Support Links
    Given User is on Forgot Password Page
    Then Verify support text is present
    And Verify support link is present


#  Scenario: Verify error message on wrong email format
#    Given User is on Forgot Password Page
#    When User enters incorrect formatted email
#    Then Verify error message is present

  Scenario: Verify info message on entering correct email
    Given User is on Forgot Password Page
    When User enters correct email address
    And User clicks on send Button
    Then Verify info message is present
    And Verify info message