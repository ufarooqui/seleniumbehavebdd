from behave import *
import time

use_step_matcher("re")


@when("User clicks on Contact Support Link")
def step_impl(context):
    context.login_page.click_contact_support_link()
    time.sleep(3)


@then("User is taken to Datavalet Support Page in new tab")
def step_impl(context):
    # switch to datavalet support tab
    context.login_page.switch_to_portal_tab(1)

    # get current url of datavalet support tab
    current_url = context.browser.current_url
    time.sleep(3)

    # close datavalet support tab
    context.login_page.close_active_tab()

    time.sleep(2)

    # switch back focus to DOT application
    context.login_page.switch_to_portal_tab(0)
    assert current_url == "https://datavalet.com/support/"
