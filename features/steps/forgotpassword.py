from behave import *
import time
from hamcrest import *

use_step_matcher("re")


@step("User clicks on Forgot Password link")
def step_impl(context):
    context.forgot_password_page.click_forgot_password_link()
    time.sleep(3)


@then('Verify heading should be Forgot Password')
def step_impl(context):
    forgot_password_heading = context.forgot_password_page.get_forgot_password_heading()
    assert(forgot_password_heading == "Forgot Password")


@step("Verify Back to Login link is displayed")
def step_impl(context):
    assert(context.forgot_password_page.verify_back_to_login_link_is_present())


@step("Verify email label should be displayed")
def step_impl(context):
    assert(context.forgot_password_page.get_forgot_password_email_label() == "Email")


@step("Verify forgot email textbox is present")
def step_impl(context):
    assert (context.forgot_password_page.verify_forgot_password_email_textbox_is_present())


@step("Verify send button is present")
def step_impl(context):
    assert (context.forgot_password_page.verify_send_button_is_present())


@given("User is on Forgot Password Page")
def step_impl(context):
    pass


@then("Verify support text is present")
def step_impl(context):
    support_text = context.forgot_password_page.get_forgot_password_support_text()
    assert_that(support_text, contains_string("If you still need help, contact support at\ndatavalet.com/support"),
                "Incorrect Text")


@step("Verify support link is present")
def step_impl(context):
    assert (context.forgot_password_page.verify_support_link_is_present())


@when("User enters incorrect formatted email")
def step_impl(context):
    context.forgot_password_page.enter_email("umair")


@then("Verify error message is present")
def step_impl(context):
    assert(context.forgot_password_page.verify_incorrect_email_error_message())


@when("User enters correct email address")
def step_impl(context):
    context.forgot_password_page.enter_email("umair.faruki@gmail.com")


@step("User clicks on send Button")
def step_impl(context):
    context.forgot_password_page.click_send_button()


@then("Verify info message is present")
def step_impl(context):
    assert(context.forgot_password_page.verify_correct_email_info_message())


@step("Verify info message")
def step_impl(context):
    info_message = context.forgot_password_page.verify_info_message()
    assert_that(info_message, contains_string("If an account exists with these credentials, instructions to reset your password will be sent to the email address associated with this account. Please check your email."),
                "Incorrect Info Message")
