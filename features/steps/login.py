from behave import *
import time
from features.pages.login_page import LoginPage

use_step_matcher("re")


@given("User is Logged out")
def step_impl(context):
    context.login_page.logout()


@given("User is on Login Page")
def step_impl(context):
    context.browser.get("https://dot-dev-northamerica-northeast1.datavalet.dev/")
    # context.login_page.logout()


@when('User enters username "(.*)"')
def step_impl(context, email):
    context.login_page.enter_email(email)


@step('User enters password "(.*)"')
def step_impl(context, password):
    context.login_page.enter_password(password)


@step("User clicks on continue Button")
def step_impl(context):
    context.login_page.click_continue_button()
    time.sleep(3)


@then("User should get logged in")
def step_impl(context):
    print("User should get logged in ")


@then("Verify Datavalet logo is present")
def step_impl(context):
    assert (context.login_page.verify_datavalet_logo_is_present())


@step("Verify email textbox is present")
def step_impl(context):
    assert (context.login_page.verify_email_textbox_is_present())


@step("Verify password textbox is present")
def step_impl(context):
    assert (context.login_page.verify_password_textbox_is_present())


@step('Verify email label should be "(.*)"')
def step_impl(context, actual_email_label):
    observed_email_label = context.login_page.get_email_label()
    assert(observed_email_label == actual_email_label)


@step('Verify password label should be "(.*)"')
def step_impl(context, actual_password_label):
    observed_password_label = context.login_page.get_password_label()
    assert(observed_password_label == actual_password_label)


@step('Verify DOT heading should be "(.*)"')
def step_impl(context, actual_dot_heading):
    observed_dot_heading = context.login_page.get_dot_heading()
    assert (observed_dot_heading == actual_dot_heading)


@step("Verify Continue Button should be present")
def step_impl(context):
    assert(context.login_page.verify_continue_button_is_present())


@step("Verify Need assistance label is present")
def step_impl(context):
    assert(context.login_page.get_need_assistance_label() == "Need assistance? Contact support")


@step("Verify Contact support link is present")
def step_impl(context):
    assert(context.login_page.verify_contact_support_link_is_present())


@step("Verify forgot password link is present")
def step_impl(context):
    assert(context.login_page.verify_forgot_password_link_is_present())