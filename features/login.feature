Feature: Login Action
  Description: This feature will test a LogIn functionality

  Scenario: Verify elements on Email login page
    Given User is Logged out
    Then Verify Datavalet logo is present
    And Verify email textbox is present
    And Verify email label should be "Email"
    And Verify DOT heading should be "Welcome to DOT"
    And Verify Contact support link is present
    And Verify Need assistance label is present
    And Verify Continue Button should be present

  Scenario: Verify elements on Password Login page
    Given User is on Login Page
    When User enters username "test5@aw.ca"
    And User clicks on continue Button
    Then Verify Datavalet logo is present
    And Verify password textbox is present
    And Verify password label should be "Password"
    And Verify DOT heading should be "Welcome to DOT"
    And Verify forgot password link is present
    And Verify Continue Button should be present

  Scenario: Login with Invalid Credentials
    Given User is on Login Page
    When User enters username "umairf6@yahoo.com"
    And User clicks on continue Button
    And User enters password "wrongpassword"
    And User clicks on continue Button
    Then User should get logged in

  Scenario: Login with valid Credentials
    Given User is on Login Page
    When User enters username "test5@aw.ca"
    And User clicks on continue Button
    And User enters password "Abc123456!"
    And User clicks on continue Button
    Then User should get logged in



#Scenario Outline: Login with valid and Invalid Credentials
#
#    Given User is on Home Page
#    When User navigate to Login Page
#    Then User enters "<username>" and "<password>"
#    And Keeping case as Valid
#    Then User should get logged in
#    And Message displayed Login Successfully
#    Then User enters "<username>" and "<password>"
#    And Keeping case as InValid
#    Then user will be asked to go back to login page
#    And Provide correct credentials

#Examples:
#        |username|password|Case|
#        |abc@gmail.com|12345|Valid|
#        |abc1@gmail.com|dfsd2|InValid|


#Scenario: Successful logout from application
#
#    When user logs out from application
#    Then Message displayed Logout successfully
#    And Browser quit by driver




