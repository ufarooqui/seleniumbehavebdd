from features.pages.base_page import BasePage
from features.locators.forgot_password_page import ForgotPasswordPageLocator
import time


class ForgotPasswordPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.browser)

    def click_forgot_password_link(self):
        self.find_element(*ForgotPasswordPageLocator.forgotPasswordLinkLocator).click()
        time.sleep(2)

    def get_forgot_password_heading(self):
        forgot_password_heading = self.find_element(*ForgotPasswordPageLocator.forgotPasswordHeadingLocator)
        return forgot_password_heading.text

    def verify_back_to_login_link_is_present(self):
        back_to_login_link = self.find_element(*ForgotPasswordPageLocator.backTologinPageLinkLocator)
        return back_to_login_link.is_displayed()

    def get_forgot_password_email_label(self):
        forgot_password_email_label = self.find_element(*ForgotPasswordPageLocator.forgotPasswordEmailLabelLocator)
        return forgot_password_email_label.text

    def verify_forgot_password_email_textbox_is_present(self):
        email_textbox = self.find_element(*ForgotPasswordPageLocator.forgotEmailIDTextBoxLocator)
        return email_textbox.is_displayed()

    def verify_send_button_is_present(self):
        send_button = self.find_element(*ForgotPasswordPageLocator.forgotPasswordSendButtonLocator)
        return send_button.is_displayed()

    def get_forgot_password_support_text(self):
        forgot_password_support = self.find_element(*ForgotPasswordPageLocator.forgotPasswordSupportTextLocator)
        return forgot_password_support.text

    def verify_support_link_is_present(self):
        support_link = self.find_element(*ForgotPasswordPageLocator.forgotPasswordSupportLinkLocator)
        return support_link.is_displayed()

    def enter_email(self, email):
        email_element = self.find_element(*ForgotPasswordPageLocator.forgotEmailIDTextBoxLocator)
        email_element.clear()
        email_element.send_keys(email)

    def verify_incorrect_email_error_message(self):
        error_message = self.find_element(*ForgotPasswordPageLocator.incorrectEmailLocator)
        return error_message.is_displayed() and error_message.text == "Incorrect email format."

    def click_send_button(self):
        self.find_element(*ForgotPasswordPageLocator.send_button_locator).click()
        time.sleep(2)

    def verify_correct_email_info_message(self):
        info_message = self.find_element(*ForgotPasswordPageLocator.info_message_locator)
        return info_message.is_displayed()

    def verify_info_message(self):
        info_message = self.find_element(*ForgotPasswordPageLocator.info_message_locator)
        return info_message.text
