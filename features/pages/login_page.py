from features.pages.base_page import BasePage
from features.locators.login_page import LoginPageLocator
import time


class LoginPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.browser)

    def login(self, username="test5@aw.ca", passwd="Abc123456!"):
        self.enter_email(username)
        self.click_continue_button()
        self.enter_password(passwd)
        self.click_continue_button()

    def enter_email(self, email):
        self.find_element(*LoginPageLocator.emailTextBoxElementLocator).send_keys(email)

    def enter_password(self, password):
        self.find_element(*LoginPageLocator.passwordTextBoxElementLocator).send_keys(password)

    def click_continue_button(self):
        self.find_element(*LoginPageLocator.submitButtonElementLocator).click()
        time.sleep(2)

    def click_contact_support_link(self):
        self.find_element(*LoginPageLocator.contactSupportLocator).click()
        time.sleep(2)

    def logout(self):
        self.find_element(*LoginPageLocator.profileLink).click()
        time.sleep(2)
        self.find_element(*LoginPageLocator.signOutLink).click()
        time.sleep(2)

    def verify_datavalet_logo_is_present(self):
        datavalet_logo = self.find_element(*LoginPageLocator.datavaletLogoLocator)
        return datavalet_logo.is_displayed()

    def verify_email_textbox_is_present(self):
        email_textbox = self.find_element(*LoginPageLocator.emailTextBoxElementLocator)
        return email_textbox.is_displayed()

    def verify_password_textbox_is_present(self):
        password_textbox = self.find_element(*LoginPageLocator.passwordTextBoxElementLocator)
        return password_textbox.is_displayed()

    def verify_continue_button_is_present(self):
        continue_button = self.find_element(*LoginPageLocator.submitButtonElementLocator)
        return continue_button.is_displayed()

    def verify_contact_support_link_is_present(self):
        support_link = self.find_element(*LoginPageLocator.contactSupportLocator)
        return support_link.is_displayed()

    def get_email_label(self):
        email_label = self.find_element(*LoginPageLocator.emailLabelLocator)
        return email_label.text

    def get_password_label(self):
        password_label = self.find_element(*LoginPageLocator.passwordLabelLocator)
        return password_label.text

    def get_need_assistance_label(self):
        need_assistance_label = self.find_element(*LoginPageLocator.needAssistanceLabelLocator)
        return need_assistance_label.text

    def get_dot_heading(self):
        dot_heading = self.find_element(*LoginPageLocator.dotHeadingLocator)
        return dot_heading.text

    def verify_forgot_password_link_is_present(self):
        forgot_password_link = self.find_element(*LoginPageLocator.forgotPasswordLinkLocator)
        return forgot_password_link.is_displayed()

    # def navigate(self, address):
    #     self.driver.get(address)
