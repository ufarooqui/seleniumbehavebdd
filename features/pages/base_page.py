from features.locators.base_page import BasePageLocators
from selenium.webdriver.support.select import Select


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def find_element(self, by, value):
        return self.driver.find_element(by, value)

    def title(self):
        return self.driver.find_element(*BasePageLocators.TITLE)

    def close_active_tab(self):
        self.driver.close()

    def switch_to_portal_tab(self, index):
        self.driver.switch_to.window(self.driver.window_handles[index])

    def switch_to_main_tab(self, current_window_handle):
        self.driver.switch_to.window(current_window_handle)

    def get_current_window_handle(self):
        return self.driver.current_window_handle

    def get_parent_handle(self):
        parent_handle = self.driver.current_window_handle
        return parent_handle

    def switch_to_current_tab(self):
        self.driver.switch_to.window(self.driver.window_handles[-1])

    @staticmethod
    def element_has_class(ele, attribute_name):
        return ele.get_attribute(attribute_name)

    @staticmethod
    def get_default_value_from_drop_down(element):
        select = Select(element)
        selected_option = select.first_selected_option
        print(selected_option.text)
        return selected_option.text
