from selenium import webdriver
import os
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import IEDriverManager


class WebDriverFactory:
    def __init__(self, data):
        self.data = data

    def get_web_driver_instance(self):
        base_url = self.data['BaseUrl']
        headless_mode = self.data['HeadLess']
        browser = self.data['Browser']

        if browser == "iexplorer":
            driver = webdriver.Ie(IEDriverManager().install())
        elif browser == "firefox":
            if headless_mode:
                options = Options()
                options.add_argument("-headless")
                driver = webdriver.Firefox(firefox_options=options, executable_path=GeckoDriverManager().install())
            else:
                driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
                driver.maximize_window()
        elif browser == "chrome":
            if headless_mode:
                chrome_options = Options()
                chrome_options.add_argument("headless")
                chrome_options.add_argument("--window-size=1920x1080")
                chrome_options.add_argument("--remote-debugging-port=9222")
                chrome_options.add_argument('--disable-gpu')  # Last I checked this was necessary.
                chrome_options.add_argument('--no-sandbox')
                driver = webdriver.Chrome(chrome_options=chrome_options,
                                          executable_path=ChromeDriverManager().install())
            else:
                driver = webdriver.Chrome(ChromeDriverManager().install())
            driver.maximize_window()
        else:
            driver = webdriver.Firefox()
        driver.implicitly_wait(3)
        # driver.maximize_window()
        driver.get(base_url)
        return driver
