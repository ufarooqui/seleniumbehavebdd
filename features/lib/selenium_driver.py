from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from traceback import print_stack
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import *
import features.utilities.custom_logger as cl
import logging
import time
import os
import allure

# from base.element_repository import ElementsRepository


class SeleniumDriver():

    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super(SeleniumDriver, self).__init__(driver)
        self.driver = driver

    def capture_screen_shot(self, name):
        self.driver.get_screenshot_as_file(name)

    def screen_shot(self, result_message):
        file_name = result_message + "." + str(round(time.time() * 1000)) + ".png"
        screen_shot_directory = "../screenshots/"  # place to save screenshots
        relative_file_name = screen_shot_directory + file_name
        current_directory = os.path.dirname(__file__)  # directory name of the current file
        destination_file = os.path.join(current_directory, relative_file_name)
        destination_directory = os.path.join(current_directory, screen_shot_directory)

        try:
            if not os.path.exists(destination_directory):
                os.makedirs(destination_directory)
            self.driver.save_screen_shot(destination_file)
            # self.full_page_screen_shot(destination_file)
            self.log.info("Screen shot save to directory " + destination_file)
        except IOError:
            self.log.error("exception occurred")
            print_stack()

    def element_click(self, locator="", locator_type="id", element=None):
        try:
            if locator:  # This means if locator is not empty
                element = self.get_element(locator, locator_type)
            time.sleep(2)
            element.click()
            self.log.info("Clicked on element with locator: " + locator +
                          " locatorType: " + locator_type)
        except WebDriverException:
            self.log.info("Cannot click on the element with locator: " + locator +
                          " locatorType: " + locator_type)
            print_stack()

    def get_title(self):
        return self.driver.title

    def enter_data_in_element(self, data, locator="", locator_type="id", element=None):
        try:
            if locator:  # This means if locator is not empty
                element = self.get_element(locator, locator_type)
            element.clear()
            element.send_keys(data)
            self.log.info("Sent data on element with locator: " + locator +
                          " locatorType: " + locator_type)
        except WebDriverException:
            self.log.info("Cannot send data on the element with locator: " + locator +
                          " locatorType: " + locator_type)
            print_stack()

    def get_by_type(self, locator_type):
        locator_type = locator_type.lower()
        if locator_type == "id":
            return By.ID
        elif locator_type == "name":
            return By.NAME
        elif locator_type == "xpath":
            return By.XPATH
        elif locator_type == "css":
            return By.CSS_SELECTOR
        elif locator_type == "class":
            return By.CLASS_NAME
        elif locator_type == "link":
            return By.LINK_TEXT
        elif locator_type == "tagname":
            return By.TAG_NAME
        else:
            self.log.info("Locator type " + locator_type + " not correct/supported")
        return False

    def get_element(self, locator, locator_type="id"):
        element = None
        try:
            locator_type = locator_type.lower()
            by_type = self.get_by_type(locator_type)
            element = self.driver.find_element(by_type, locator)
            self.log.info("Element Found with Locator:" + locator + " Locator Type: " + locator_type)
        except NoSuchElementException:
            self.log.info("Element not found with Locator:" + locator + " Locator Type: " + locator_type)
        return element

    def get_elements(self, locator, locator_type="id"):
        """
        NEW METHOD
        Get list of elements
        """
        element = None
        try:
            locator_type = locator_type.lower()
            by_type = self.get_by_type(locator_type)
            element = self.driver.find_elements(by_type, locator)
            self.log.info("Element list found with locator: " + locator +
                          " and  locatorType: " + locator_type)
        except NoSuchElementException:
            self.log.info("Element list not found with locator: " + locator +
                          " and  locatorType: " + locator_type)
        return element

    def get_text(self, locator="", locator_type="id", element=None, info=""):
        try:
            if locator:  # This means if locator is not empty
                self.log.debug("In locator condition")
                element = self.get_element(locator, locator_type)
            self.log.debug("Before finding text")
            text = element.text
            self.log.debug("After finding element, size is: " + str(len(text)))
            if len(text) == 0:
                text = element.get_attribute("innerText")
            if len(text) != 0:
                self.log.info("Getting text on element :: " + info)
                self.log.info("The text is :: '" + text + "'")
                text = text.strip()
        except (NoSuchAttributeException, NoSuchElementException):
            self.log.error("Failed to get text on element " + info)
            print_stack()
            text = None
        return text

    def get_value_from_textbox(self, locator, locator_type="id", element=None, info=""):
        try:
            if locator:  # This means if locator is not empty
                self.log.debug("In locator condition")
                element = self.get_element(locator, locator_type)
            self.log.debug("Before finding text from textbox")
            get_input_text = element.get_attribute("value")
            self.log.info("Getting text on input element :: " + info)
            self.log.info("The text is :: '" + get_input_text + "'")
            get_input_text = get_input_text.strip()
        except (NoSuchAttributeException, NoSuchElementException):
            self.log.error("Failed to get text from textbox " + info)
            print_stack()
            get_input_text = None
        with allure.step('Value from textbox {0}'.format(get_input_text)):
            return get_input_text

    @staticmethod
    def is_attribute_present(element=None):
        attribute_value = element.get_attribute("innerHTML")
        if 'value' in attribute_value:
            return True
        else:
            return False

    def is_element_present(self, locator="", locator_type="id", element=None):
        try:
            if locator:  # This means if locator is not empty
                element = self.get_element(locator, locator_type)
            if element is not None:
                self.log.info("Element present with locator: " + locator +
                              " locatorType: " + locator_type)
                return True
            else:
                self.log.info("Element not present with locator: " + locator +
                              " locatorType: " + locator_type)
                return False
        except NoSuchElementException:
            print("Element not found")
            return False

    def element_presence_check(self, locator, by_type):
        try:
            element_list = self.driver.find_elements(by_type, locator)
            if len(element_list) > 0:
                self.log.info("Element present with locator: " + locator +
                              " locatorType: " + str(by_type))
                return True
            else:
                self.log.info("Element not present with locator: " + locator +
                              " locatorType: " + str(by_type))
                return False
        except NoSuchElementException:
            self.log.info("Element not found")
            return False

    def verify_all_links(self):
        try:
            element_list = self.get_elements("a", "tagname")
            for ele in element_list:
                if ele.get_attribute("href") is None:
                    self.log.info("Link not found")
                    with allure.step('Result: Link Not Found'):
                        return False
            self.log.info("No Broken Links")
            with allure.step('Result: No Broken Links'):
                return True
        except (NoSuchAttributeException, NoSuchElementException, TimeoutException):
            with allure.step('Result: Exception Occurred'):
                self.log.info("Link not found")
                return False

    def is_element_displayed(self, locator="", locator_type="id", element=None):
        is_displayed = False
        try:
            if locator:  # This means if locator is not empty
                element = self.get_element(locator, locator_type)
            if element is not None:
                is_displayed = element.is_displayed()
                self.log.info("Element is displayed with locator: " + locator +
                              " locatorType: " + locator_type)
            else:
                self.log.info("Element not displayed with locator: " + locator +
                              " locatorType: " + locator_type)
            return is_displayed
        except (NoSuchElementException, ElementNotVisibleException):
            print("Element not found")
            return False

    def web_scroll(self, direction="up"):
        """
        NEW METHOD
        """
        if direction == "up":
            # Scroll Up
            self.driver.execute_script("window.scrollBy(0, -1000);")

        if direction == "down":
            # Scroll Down
            self.driver.execute_script("window.scrollBy(0, 1000);")

    def wait_for_element(self, locator, locator_type="id", timeout=10, poll_frequency=0.5):
        element = None
        try:
            by_type = self.get_by_type(locator_type)
            self.log.info("Waiting for maximum :: " + str(timeout) +
                          " :: seconds for element to be clickable")
            wait = WebDriverWait(self.driver, timeout=timeout,
                                 poll_frequency=poll_frequency,
                                 ignored_exceptions=[NoSuchElementException,
                                                     ElementNotVisibleException,
                                                     ElementNotSelectableException])
            element = wait.until(ec.element_to_be_clickable((by_type, locator)))
            self.log.info("Element is clickable on the web page")
        except (ElementNotSelectableException, TimeoutException):
            self.log.info("Element not clickable on the web page")
            print_stack()
        return element

    def wait_for_element_to_be_present(self, locator, locator_type="id", timeout=10, poll_frequency=0.5):
        element = None
        try:
            by_type = self.get_by_type(locator_type)
            self.log.info("Waiting for maximum :: " + str(timeout) +
                          " :: seconds for element to be clickable")
            wait = WebDriverWait(self.driver, timeout=timeout,
                                 poll_frequency=poll_frequency,
                                 ignored_exceptions=[NoSuchElementException])
            element = wait.until(ec.presence_of_element_located((by_type, locator)))
            self.log.info("Element appeared on the web page")
        except TimeoutException:
            self.log.info("Element not appeared on the web page")
        return element

    def wait_for_element_to_disappear(self, locator, locator_type="id", timeout=20, poll_frequency=0.5):
        element = None
        try:
            by_type = self.get_by_type(locator_type)
            self.log.info("Waiting for maximum :: " + str(timeout) +
                          " :: seconds for element to be clickable")
            wait = WebDriverWait(self.driver, timeout=timeout,
                                 poll_frequency=poll_frequency,
                                 ignored_exceptions=[TimeoutException])
            element = wait.until_not(ec.presence_of_element_located((by_type, locator)))
            self.log.info("Element disappeared from the web page")
        except NotImplementedError:
            self.log.info("Element did not disappear from the web page")
        return element