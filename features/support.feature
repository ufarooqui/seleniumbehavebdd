Feature: Contact Support
  Description: This feature will test a Contact Support Link

  Scenario: Verify Contact Support Link on login page
    Given User is Logged out
    When User clicks on Contact Support Link
    Then User is taken to Datavalet Support Page in new tab