# Behave BDD Installation 

#### Python version

```3.6```

#### Install Selenium

```
pip install selenium
``` 

#### Install Behave
Execute the following command to install behave with pip:
``` 
pip install behave
pip install -U behave (update)
```
 
#### Install Webdriver Manager 

```
pip install webdriver-manager
```

#### Install Allure For Reporting 

```
pip install allure-behave
```

#### Project Layout 
- Create a directory called "features". In that directory put all your feature files.
- Create a new directory "features/steps". In that directory put all your step files.

The minimum requirement for a features directory is:
```
features/
features/everything.feature
features/steps/
features/steps/steps.py
```

> **Note: You may optionally include some environmental controls (code to run before and after steps, scenarios, features
or the whole shooting match).**


#### Scenario Outlines
Sometimes a scenario should be run with a number of variables giving a set of known states, actions to take and
expected outcomes, all using the same basic actions. You may use a Scenario Outline to achieve this:

```
Scenario Outline: Blenders
    Given I put <thing> in a blender,
    when I switch the blender on
    then it should transform into <other thing>

Examples: Amphibians
    | thing | other thing |
    | Red Tree Frog | mush |
Examples: Consumer Electronics
    | thing | other thing |
    | iPhone | toxic waste |
    | Galaxy Nexus | toxic waste |
```
behave will run the scenario once for each (non-heading) line appearing in the example data tables.

#### Run Behave 
```
behave -v
```